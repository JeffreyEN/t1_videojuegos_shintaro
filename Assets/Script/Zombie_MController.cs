using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_MController : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    public int VelocidadDeMovimiento;
    public float upSpeed = 60;
    bool EstaTocandoElSuelo = false;
    bool inicio = false;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Space))
        {
            inicio = true;
        }
        if (inicio)
        {
            sr.flipY = false;
            if (Input.GetKey(KeyCode.AltGr))
            {
            }
            else
            {
                if (EstaTocandoElSuelo)
                {
                    setJumpAnimation();
                    rb2d.velocity = new Vector2(VelocidadDeMovimiento, rb2d.velocity.y);
                    rb2d.velocity = Vector2.up * upSpeed;
                    EstaTocandoElSuelo = false;
                }
            }

        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }


}
