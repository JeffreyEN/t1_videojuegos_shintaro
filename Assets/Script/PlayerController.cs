using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    private int Puntos = 0;
    public float VelocidadDeMovimiento;
    public float upSpeed = 60;
    bool EstaTocandoElSuelo = false;
    bool inicio = false;
    bool Muerto = false;
    

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            inicio = true;
        }
        if (inicio)
        {
            //float upSpeed = 60;
            if (Input.GetKey(KeyCode.Space) && EstaTocandoElSuelo && Muerto == false)
            { //Derecha
            setJumpAnimation();
            EstaTocandoElSuelo = false;
            Debug.Log("entro");
                rb2d.velocity = Vector2.up * upSpeed;
            }
            else
            {
                setRunAnimation();
                rb2d.velocity = new Vector2(VelocidadDeMovimiento, rb2d.velocity.y);
            }
            if (Puntos == 10)
            {

                VelocidadDeMovimiento = (float)(VelocidadDeMovimiento * 1.3);
                Puntos = 0;
            }
            if (Muerto == true)
            {
                setMorirAnimation();
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
            
        }
        else
        {
            setIdleAnimation();
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;

        
        if (other.gameObject.layer == 6)
        {
            Muerto = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Square_H")
        {
            Puntos++;
            Debug.Log(Puntos);
        }
    }

    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setMorirAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }
}
